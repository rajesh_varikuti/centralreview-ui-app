import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import Button from '@material-ui/core/Button';

import { Link, Input } from "@material-ui/core";

import Header from "../CR_Common/Header";
import Footer from "../CR_Common/Footer";

import './AddNewRuleComponent.css';

const useStyles = makeStyles(theme => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 130
  },
  buttonControl: {
    margin: theme.spacing(2.5, 1, 0, 1),
    minWidth: 120
  }, 
  root: {
    width: 900,
    margin: theme.spacing(3, 23)
  },
  container: {
    maxHeight: 340,
  },
  ruleContainer: {
    width: 900,
    margin: theme.spacing(3, 23),
    border :theme.typography,
    height: 400,
  },
}));

function CreateNewRule() {
  const classes = useStyles();

  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };
  return (
    <div>
      <Header/>
    
    <div styles= " background-color: #eee;" className={classes.ruleContainer}>
      <h3>New CR Rule Creation</h3>
      <FormControl className={classes.formControl}>
        <InputLabel htmlFor="grouped-native-select">Category</InputLabel>
        <Select native defaultValue="" id="grouped-native-select">
          <option aria-label="None" value="" />
          <option value={1}>Administration</option>
          <option value={2}>Parts</option>
          <option value={3}>Rates</option>
          <option value={4}>Estimate Line</option>
        </Select>
      </FormControl>
      <FormControl className={classes.formControl}>
        <InputLabel htmlFor="grouped-select">Sub Category</InputLabel>
        <Select defaultValue="" id="grouped-select">
          <option aria-label="None" value="" />
          <option value={1}>Vehicle</option>
          <option value={2}>Date</option>
          <option value={3}>Custmer</option>
          <option value={4}>Damage</option>
          <option value={4}>Part Selection</option>
        </Select>
      </FormControl>
      <FormControl className={classes.formControl}>
        <InputLabel htmlFor="grouped-select">Operations</InputLabel>
        <Select defaultValue="" id="grouped-select">
          <option aria-label="None" value="" />
          <option value={1}>and</option>
          <option value={2}>or</option>
          <option value={3}>before</option>
          <option value={4}>greater than</option>
          <option value={4}>equals to</option>
        </Select>
      </FormControl>
      <FormControl className={classes.formControl}>
        <InputLabel htmlFor="grouped-select">Amount</InputLabel>
        <Input></Input>
      </FormControl>
      <FormControl className={classes.formControl}>
        <InputLabel htmlFor="grouped-select">Location</InputLabel>
        <Select defaultValue="" id="grouped-select">
          <option aria-label="None" value="" />
          <option value={1}>Boston</option>
          <option value={2}>Atlanta</option>
          <option value={3}>O'Fallon</option>
          <option value={4}>San Diego</option>
        </Select>
      </FormControl>
      <FormControl className={classes.formControl}>
        <InputLabel htmlFor="grouped-select">Value</InputLabel>
        <Input></Input>
      </FormControl>
      <FormControl className={classes.formControl}>
        <InputLabel htmlFor="grouped-select">Advisor</InputLabel>
        <Select defaultValue="" id="grouped-select">
          <option aria-label="None" value="" />
          <option value={1}>Boston</option>
          <option value={2}>Atlanta</option>
          <option value={3}>O'Fallon</option>
          <option value={4}>San Diego</option>
        </Select>
      </FormControl>
      <FormControl className={classes.formControl}>
        <InputLabel htmlFor="grouped-select">Description</InputLabel>
        <Input></Input>
      </FormControl>
      <div>
      <FormControl className={classes.buttonControl}>
      <Button variant="contained" color="primary">
        Validate Rule
      </Button>
      </FormControl>
      <FormControl className={classes.buttonControl}>
      <Button variant="contained" color="primary">
        Save
      </Button>
      </FormControl>
      <FormControl className={classes.buttonControl}>
      <Button variant="contained" color="secondary">
        Reset
      </Button>
      </FormControl></div>
      
     
    </div>
    <Footer/>
    </div>
  
  );
          }

export default CreateNewRule;
