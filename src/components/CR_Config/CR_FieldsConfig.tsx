import React, {Component} from 'react';
import merge from 'lodash/merge';

import {
    BasicConfig, Fields, Config,
} from 'react-awesome-query-builder';

import AntdConfig from 'react-awesome-query-builder/lib/config/antd';
import AntdWidgets from 'react-awesome-query-builder/lib/components/widgets/antd';
const {
    FieldSelect,
    FieldDropdown,
    FieldCascader,
    FieldTreeSelect,
} = AntdWidgets;

const fieldsContent: Fields = {   
        
    RepairOrderHeader: {
        label: 'Estimate',
        type: '!group',
        subfields: {
            'AdminInfo.RepairFacility.Party.OrgInfo.IDinfo.IDQualifierCode': {
                label: 'Location',
                type: 'select',
                // operators: [ 'equal',
                // 			 'in',
                //     ],
                listValues: [
                    // location id = pk id no need to join / db call
                  { value: 'all', title: 'All' },
                  { value: '1000', title: 'Elmhurst, IL' },
                  { value: '1001', title: 'Schauburg, IL' }
    ],
            },
            'AdminInfo.InsuranceCompany.Party.OrgInfo.IDInfo.IDQualifierCode': {
                label: 'Insurance Carrier',
                type: 'select',
                // operators: [ 'equal',
                //     'in',
                //     ],
                valueSources: ['value'],
                listValues: [
                  { value: 'ALL', title: 'All State' },
                  { value: 'SFI', title: 'State Farm Insurance' },
                  { value: 'USA', title: 'USAA' },
                ],
            },
            'AdminInfo.InsuranceCompany.Party.ContactInfo.ContactName.FirstName': {
                label: 'Estimator - First Name',
                type: 'text',
                operators: [ 'equal',                       
                    'like',
                    ],
                valueSources: ['value'],
            },
            'AdminInfo.InsuranceCompany.Party.ContactInfo.ContactName.LastName': {
                label: 'Estimator - Last Name',
                type: 'text',
                operators: [ 'equal',
                    'like',
                    'not_like',
                    ],
                valueSources: ['value'],
            },
        }
    },
    RepairTotalsInfo: {
        label: 'Repair Totals',
        type: '!group',
        
        subfields: {
           // 'SummaryTotalsInfo.TotalType': {'TOT'},
        //    'SummaryTotalsInfo.TotalSubType': {'TT'},
    		'SummaryTotalsInfo.TotalAmt': {
                label: 'Adjustment Type',
                type: 'text',
    			
                valueSources: ['value'],
            },
        }
    },
    DamageLineInfo: {
        label: 'Estimate Lines',
        type: '!group',
        
        subfields: {
            'PartInfo.PriceAdjustment.AdjustmentType': {
                label: 'Adjustment Type',
                type: 'select',
               
                valueSources: ['value'],
                listValues: [
                  { value: 'BTR', title: 'Betterment' },
                  { value: 'DIS', title: 'Discount' },
                  { value: 'MAR', title: 'Markup' }
                ],
            },
            'PartInfo.PriceAdjustment.AdjustmentPct': {
                label: 'Adjustment Pct',
                type: 'text',
                operators: [ 'equal',
                    'not_equal',
                    'less',
                    'less_or_equal',
                    'greater',
                    'greater_or_equal',
                    'between',],
                valueSources: ['value'],
            },
        }
    },
    // results: {
    //     label: 'Results',
    //     type: '!group',
    //     subfields: {
    //         product: {
    //             type: 'select',
    //             listValues: ['abc', 'def', 'xyz'],
    //             valueSources: ['value'],
    //         },
    //         score: {
    //             type: 'number',
    //             fieldSettings: {
    //                 min: 0,
    //                 max: 100
    //             },
    //             valueSources: ['value'],
    //         }
    //     }
    // },
    };
export default fieldsContent;