export default 
{
  "type": "group",
  "id": "Estimate",
  "children1": {
    "RepairOrderHeader": {
      "type": "rule_group",
      "properties": {
        "conjunction": "AND",
        "field": "RepairOrderHeader"
      },
      "children1": {
        "AdminInfo.RepairFacility.Party.OrgInfo.IDinfo.IDQualifierCode": {
          "type": "rule",
          "properties": {
            "field": "RepairOrderHeader.AdminInfo.RepairFacility.Party.OrgInfo.IDinfo.IDQualifierCode",
            "operator": "select_equals",
            "value": [
              "Elmurst, IL"
            ],
            "valueSrc": [
              "Elmurst, IL"
            ],
            "valueType": [
              "select"
            ]
          }
        },
        "AdminInfo.InsuranceCompany.Party.OrgInfo.IDInfo.IDQualifierCode": {
          "type": "rule",
          "properties": {
            "field": "results.score",
            "operator": "greater",
            "value": [
              8
            ],
            "valueSrc": [
              "value"
            ],
            "valueType": [
              "number"
            ]
          }
        }
      }
    }
  },
  "properties": {
    "conjunction": "AND",
    "not": false
  }
}