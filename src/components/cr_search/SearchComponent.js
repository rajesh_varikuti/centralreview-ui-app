import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import Button from '@material-ui/core/Button';

import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import { Link } from "@material-ui/core";

import Header from "../CR_Common/Header";
import Footer from "../CR_Common/Footer";

const columns = [
  { id: 'insuranceCarrier', label: 'Insurance Carrier', minWidth: 170 },
  { id: 'location', label: 'Location', minWidth: 100 },
  {
    id: 'rules',
    label: 'Rules',
    minWidth: 170,
    align: 'center',
    format: (value) => value.toLocaleString('en-US'),
  },
];

function createData(insuranceCarrier, location, rules) {
  return { insuranceCarrier, location, rules };
}

const rows = [
  createData('Gieco', 'Atlanta', "Insurance carrier Gieco and StateFarm location = San Diego "),
  createData('Progressive', 'Boston',  "Insurance carrier Gieco and StateFarm location = San Diego "),
  createData('StateFarm', 'Atlanta',  "Insurance carrier Gieco and StateFarm location = San Diego "),
  createData('Gieco', 'San Diego',  "Insurance carrier Gieco and StateFarm location = San Diego "),
  
];

const useStyles = makeStyles(theme => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 220
  },
  buttonControl: {
    margin: theme.spacing(2.5, 1, 0, 1),
    minWidth: 120
  }, 
  root: {
    width: 900,
    margin: theme.spacing(3, 23)
  },
  container: {
    maxHeight: 340,
  },
  searchDiv: {
    margin: theme.spacing(2.5, 1, 3, 1),
  }
}));

function Search() {
  const classes = useStyles();

  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };
  return (
    
    <div>
      <Header/>
      <div className={classes.searchDiv}>
      <FormControl className={classes.formControl}>
        <InputLabel htmlFor="grouped-native-select">Insurance Carrier</InputLabel>
        <Select native defaultValue="" id="grouped-native-select">
          <option aria-label="None" value="" />
          <option value={1}>Gieco</option>
          <option value={2}>StateFarm</option>
          <option value={3}>Prograssive</option>
          <option value={4}>All States</option>
        </Select>
      </FormControl>
      <FormControl className={classes.formControl}>
        <InputLabel htmlFor="grouped-select">Location</InputLabel>
        <Select defaultValue="" id="grouped-select">
          <option aria-label="None" value="" />
          <option value={1}>Boston</option>
          <option value={2}>Atlanta</option>
          <option value={3}>O'Fallon</option>
          <option value={4}>San Diego</option>
        </Select>
      </FormControl>
      <FormControl className={classes.buttonControl}>
      <Button variant="contained" color="primary">
        Search
      </Button>
      </FormControl>
      <FormControl className={classes.buttonControl}>
      <Button variant="contained" color="secondary">
        Add New Rule
      </Button>
      </FormControl>
      </div>
      <h3>Central review Search Results</h3>
      <Paper className={classes.root}>
     

     
     <TableContainer className={classes.container}>
       <Table stickyHeader aria-label="sticky table">
         <TableHead>
           <TableRow>
             {columns.map((column) => (
               <TableCell
                 key={column.id}
                 align={column.align}
                 style={{ minWidth: column.minWidth }}
               >
                 {column.label}
               </TableCell>
             ))}
           </TableRow>
         </TableHead>
         <TableBody>
           {rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row) => {
             return (
               <TableRow hover role="checkbox" tabIndex={-1} key={row.code}>
                 {columns.map((column) => {
                   const value = row[column.id];
                   return (
                     <TableCell key={column.id} align={column.align}>
                       
                    <Link>  {column.format && typeof value === 'number' ? column.format(value) : value}
                    </Link>                      </TableCell>
                   );
                 })}
               </TableRow>
             );
           })}
         </TableBody>
       </Table>
     </TableContainer>
     <TablePagination
       rowsPerPageOptions={[10, 25, 100]}
       component="div"
       count={rows.length}
       rowsPerPage={rowsPerPage}
       page={page}
       onChangePage={handleChangePage}
       onChangeRowsPerPage={handleChangeRowsPerPage}
     />
   </Paper>
   <Footer/>
    </div>
    
  
  );
          }

export default Search;
