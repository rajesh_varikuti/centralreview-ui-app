import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';
import logo from '../../logo.gif';

const useStyles = makeStyles((theme) => ({
  toolbar: {
    borderTop: '18px solid #00529b',
    borderBottom: `1px solid #00529b`,
    width:950,
    textAlign:'center',
    margin: theme.spacing(0, 0, 0, 18),
  },
  toolbarTitle: {
    flex: 1,
  },
  toolbarSecondary: {
    justifyContent: 'space-between',
    overflowX: 'auto',
  },
  toolbarLink: {
    padding: theme.spacing(1),
    flexShrink: 0,
  },
  headeral: {
    padding: theme.spacing(1),
    flexShrink: 0,
    textDecoration: 'underline',
    fontSize: 'small',
  },
  imageSize: {
    height: 60,
  },
}));

function Header() {
  const classes = useStyles();

  return (
    <React.Fragment>
      <Toolbar className={classes.toolbar}>
      <img src={logo} alt="Logo" className={classes.imageSize}/> <h6>Central Review Rules Engine</h6>
        <Typography
          component="h2"
          variant="h5"
          color="inherit"
          align="center"
          noWrap
          className={classes.toolbarTitle} >
        </Typography>
        
        <Link className={classes.headeral}>
          Rajesh Varikuti, San Diego
        </Link>
        <Link className={classes.headeral}>
          Sign out
        </Link>
      </Toolbar>
  
    </React.Fragment>
    
  );
}

export default Header;
