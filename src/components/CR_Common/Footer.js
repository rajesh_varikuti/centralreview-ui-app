import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Toolbar from '@material-ui/core/Toolbar';
import Link from '@material-ui/core/Link';

const useStyles = makeStyles((theme) => ({
  toolbar: {
    borderTop: `1px solid #00529b`,
    borderBottom: '18px solid #00529b',
    width:950,
    textAlign:'center',
    margin: theme.spacing(0, 0, 0, 18),
    height: 1,
  },
  headeral: {
    padding: theme.spacing(1),
    flexShrink: 0,
    textDecoration: 'underline',
    fontSize: 'small',
    textOverflow:'center',
    margin: theme.spacing(0, 0, 0, 54),
  },
}));

function Footer() {
  const classes = useStyles();

  return (
    <React.Fragment>
      <Toolbar className={classes.toolbar}>
      
        
        <Link className={classes.headeral}>
          @Powered By Gerber
        </Link>
      </Toolbar>
  
    </React.Fragment>
    
  );
}

export default Footer;
