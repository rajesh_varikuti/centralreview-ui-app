import axios from 'axios';

const CR_API_BASE_URL = 'http://localhost:8080/api/centralreview/ruleconfig';

const CR_RULE_CREATE_URL = 'http://localhost:8080/api/centralreview/createrule';

class CRApiService {

    fetchRulesConfig() {
        return axios.get(CR_API_BASE_URL);
    }

    fetchRulesConfigById(ruleId) {
        return axios.get(CR_API_BASE_URL + '/' + ruleId);
    }

    createRuleAndSave(jsonFormatRule, humanStringFormatRule){
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({humanStringFormatRule, jsonFormatRule})
          };
          fetch(CR_RULE_CREATE_URL, requestOptions) .then(response => {
            if (!response.ok) {
              throw new Error("HTTP error, status = " + response.status);
            } 
            return response.json();
          }).catch(error => {
            console.log(error.message);
          })
    }

    updateRuleAndSave(){
      let query = 170;
      let url = 'http://localhost:8080/api/centralreview/rule/' + query;
      const requestOptions = {
          method: 'GET',
          headers: { 'Content-Type': 'application/json' }
        };
        fetch(url, requestOptions).then(response => {
          if (!response.ok) {
            throw new Error("HTTP error, status = " + response.status);
          } 
          return response.json();
        }).catch(error => {
          console.log(error.message);
        })
  }

}

export default new CRApiService();