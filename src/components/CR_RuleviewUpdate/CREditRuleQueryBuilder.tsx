import React, {Component} from 'react';
import {
  Query, Builder, Utils, 
  ImmutableTree, Config, BuilderProps, JsonTree, JsonLogicTree
} from 'react-awesome-query-builder';
import throttle from 'lodash/throttle';
import loadConfig from '../CR_Config/CR_ConfigTypes';
import loadedInitValue from '../CR_Common/init_value';
import loadedInitLogic from '../CR_Common/init_logic';

import Header from "../CR_Common/Header";
import Footer from "../CR_Common/Footer";

import '../../css/antd.less';
import '../../css/styles.scss';
import '../../css/denormalize.scss';
import '../../css/compact_styles.scss';

import Button from '@material-ui/core/Button';

import ApiService from "../../service/CRApiService";
const stringify = JSON.stringify;
const {queryBuilderFormat, jsonLogicFormat, queryString, mongodbFormat, sqlFormat, getTree, checkTree, loadTree, uuid, loadFromJsonLogic} = Utils;
const preStyle = { backgroundColor: 'darkgrey', margin: '10px', padding: '10px' };
const preErrorStyle = { backgroundColor: 'lightpink', margin: '10px', padding: '10px' };

const initialSkin = "antd";
const emptyInitValue: JsonTree = {id: uuid(), type: "group"};
const loadedConfig = loadConfig(initialSkin);
let initValue: JsonTree = loadedInitValue && Object.keys(loadedInitValue).length > 0 ? loadedInitValue as JsonTree : emptyInitValue;
let initTree: ImmutableTree;
initTree = checkTree(loadTree(initValue), loadedConfig);
//initTree = checkTree(loadFromJsonLogic(initLogic, loadedConfig), loadedConfig); // <- this will work same  

const updateEvent = new CustomEvent('update', { detail: {
  config: loadedConfig,
  _initTree: initTree,
  _initValue: initValue,
} });
window.dispatchEvent(updateEvent);


interface DemoQueryBuilderState {
  tree: ImmutableTree;
  config: Config;
  skin: String,
}

export default class CRQueryBuilder extends Component<{}, DemoQueryBuilderState> {
    private immutableTree !: ImmutableTree;
    private config !: Config;

    componentDidMount() {
      window.addEventListener('update', ((this.onConfigChanged)) as EventListener);
    }

    componentWillUnmount() {
      window.removeEventListener('update', ((this.onConfigChanged)) as EventListener);
    }

    state = {
      tree: initTree, 
      config: loadedConfig,
      skin: initialSkin
    };

    getRulesConfig = () => {
      ApiService.fetchRulesConfig().then(response => {
    
        console.log(response);
        return response.data; 
      });
    };

    render = () => (

      
      <div>
        <Header/>
        <Query
            {...this.state.config}
            value={this.state.tree}
            onChange={this.onChange}
            renderBuilder={this.renderBuilder}
        />
        <span><Button variant="contained" color="primary" onClick={this.resetValue}>Reset</Button></span>
        <span className = "builder-buttons"><Button variant="contained" color="primary" onClick={this.clearValue}>Clear</Button></span>
        <span className = "builder-buttons"><Button variant="contained" color="primary" onClick={this.updateRule}>Update</Button></span>
        
        <div className="query-builder-result">
          {this.renderResult(this.state)}
        </div>
        <Footer/>
      </div>
    )

    updateRule = () => {
      
      {this.editResults()}
    }

    editResults = () => {
      const staticRuleId = 99;

      ApiService.updateRuleAndSave()
     
      return (
        <div>
          <br />
          <hr/>
          <div >
            <span> data Saved Successfully..</span>
          </div>
        </div>
      )
    }

    onConfigChanged = ({detail: {config, _initTree, _initValue}}: CustomEvent) => {
      this.setState({
        config,
      });
      initTree = _initTree;
      initValue = _initValue;
    }

    resetValue = () => {
      this.setState({
        tree: initTree, 
      });
    };

    clearValue = () => {
      this.setState({
        tree: loadTree(emptyInitValue), 
      });
    };

    renderBuilder = (props: BuilderProps) => (
      <div className="query-builder-container" style={{padding: '10px'}}>
          <div className="query-builder qb-lite">
              <Builder {...props} />
          </div>
      </div>
    )
    
    onChange = (immutableTree: ImmutableTree, config: Config) => {
      this.immutableTree = immutableTree;
      this.config = config;
      this.updateResult();
      const jsonTree = getTree(immutableTree); //can be saved to backend
    }

    updateResult = throttle(() => {
      this.setState({tree: this.immutableTree, config: this.config});
    }, 100)

    renderResult = ({tree: immutableTree, config} : {tree: ImmutableTree, config: Config}) => {
      const {logic, data, errors} = jsonLogicFormat(immutableTree, config);
      return (
      <div>
        <br />
        
        <hr/>
       
        <div className="query-builder-json">

            { !!logic &&
              <pre style={preStyle}>
                // Rule:<br />
                {stringify(logic, undefined, 2)}
                <br />
               
              </pre>
            }
        </div>
      </div>
      )
  }

}