import React from 'react';
import './App.css';

import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import LoginComponent from "./components/CR_Login/LoginComponent";
import SearchComponent from "./components/CR_Search/SearchComponent";
import AddNewRuleComponent from "./components/CR_RulesCreation/AddNewRuleComponent";
import ViewEditRuleComponent from "./components/CR_RuleviewUpdate/ViewEditRuleComponent";
import CREditRuleQueryBuilder from "./components/CR_RuleviewUpdate/CREditRuleQueryBuilder";
import InactiveRulesListComponent from "./components/CR_RulesInactive/InactiveRulesListComponent";
import Header from "./components/CR_Common/Header";
import Footer from "./components/CR_Common/Footer";

import CRQueryBuilder from "./components/CR_QueryBuilder/CRQueryBuilder";

function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route path="/header" exact component={Header} />
          <Route path="/login" exact component={LoginComponent} />
          <Route path="/search" component={SearchComponent} />
          <Route path="/create-rules" component={AddNewRuleComponent} />
          <Route path="/edit-rule" component={ViewEditRuleComponent} />
          <Route path="/editRule" component={CREditRuleQueryBuilder} />
          <Route path="/inactive-rules" component={InactiveRulesListComponent} />
          <Route path="/footer" exact component={Footer} />
          <Route path="/new-rule" exact component={CRQueryBuilder} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
